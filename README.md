# Componente Calculadora FaleMais

Este projeto tem o objetivo de mostrar o componente Calculadora FaleMais.

O componente pode ser utilizado em qualquer aplicação ReactJS, para isso basta copiar a pasta 'Calculadora' para o projeto desejado.

Ultizei o ReactJS para um desenvolvimento agil e ter como resultada um componente idependente da aplicação.

Bootstrap foi utilizado para estilização.

Os teste automatizados foram desenvolvidos utilizando os framwework Jest e Enzyme(Especifico para teste de componentes isolados).

### Instalando

Instalação de dependências.

```
npm install
```

## Iniciando testes

Teste automatizados diretamenta no componente pode ser executados com o seguinte comando:

```
npm test Calculadora
```

### Iniciando a aplicação

Executa a aplicação em modo de desenvolvimento.

```
npm start
```

## Autor

**CAIO DIODI FERNANDES** 



