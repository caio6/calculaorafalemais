import React, { Component } from "react";
import { throwStatement } from "@babel/types";

import "./Calculadora.css";

// import { Container } from './styles';

export default class Calculadora extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dddA: this.props.dddA,
      dddB: this.props.dddB,
      tempo: this.props.tempo,
      plano: this.props.plano,
      ligacaoValor: 0.0,
      ligacaoValorSemPromo: 0.0,
      prices: [
        {
          dddA: "011",
          dddB: "016",
          valor: 1.9
        },
        {
          dddA: "011",
          dddB: "017",
          valor: 1.7
        },
        {
          dddA: "011",
          dddB: "018",
          valor: 0.9
        }
      ]
    };
    this.submit = this.submit.bind(this);
    this.handleSelectChange = this.handleSelectChange.bind(this);
  }

  //Calcula o valor do ligação
  submit() {
    if(!this.state.plano)
      return this.setState({ ligacaoValor: "-", ligacaoValorSemPromo: "-" });
    
    let found;
    let captalTo = false;
    found = this.state.prices.find(element => {
      return (
        element.dddA === this.state.dddA && element.dddB === this.state.dddB
      );
    });
   
    if (!found) {
      found = this.state.prices.find(element => {
        return (
          element.dddA === this.state.dddB && element.dddB === this.state.dddA
        );
      });
      if (found) {
        captalTo = false;
      }else {
        this.setState({ ligacaoValor: "-", ligacaoValorSemPromo: "-" });
      }
    } else {
      captalTo = true;
    }
    let callTime = this.state.tempo - parseFloat(this.state.plano);
    

    if (found) {
      if (callTime <= 0) {
        this.setState({ ligacaoValor: parseFloat(0), ligacaoValorSemPromo: parseFloat(0) });
      } else {
        if (captalTo) {
          this.setState({ ligacaoValor: callTime * (found.valor * 1.1), ligacaoValorSemPromo:  this.state.tempo * (found.valor) });
        } else {
          this.setState({ ligacaoValor: callTime * ((found.valor + 1) * 1.1), ligacaoValorSemPromo:  this.state.tempo * (found.valor+1) });
        }
      }
    } 

    
  }

  handleSelectChange(event) {
    this.setState({ plano: event.target.value });
  }

  showResult(result) {
    if (typeof result == "number")
      return result.toLocaleString("pt-BR", {
        style: "currency",
        currency: "BRL"
      });
    else return "R$ " + result;
  }

  render() {
    return (
      <div className="card text-center p-10">
        <div className="card-body text-dark">
          <h2 className="card-title font-weight-bold">Calculadora FaleMais</h2>
          <div className="row">
            <div className="col-12">
              <div className="input-group input-group-lg mb-3">
                <div className="input-group-prepend">
                  <span className="input-group-text">DDD</span>
                </div>
                <input
                  type="text"
                  className="form-control"
                  value={this.state.dddA}
                  onChange={event => {
                    this.setState({ dddA: event.target.value });
                  }}
                ></input>
                <input
                  type="text"
                  className="form-control"
                  value={this.state.dddB}
                  onChange={event => {
                    this.setState({ dddB: event.target.value });
                  }}
                ></input>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <div className="input-group input-group-lg mb-3">
                <div className="input-group-prepend">
                  <span className="input-group-text">Tempo da ligação</span>
                </div>
                <input
                  type="text"
                  className="form-control text-right"
                  aria-label="Large"
                  aria-describedby="inputGroup-sizing-sm"
                  value={this.state.tempo}
                  onChange={event => {
                    this.setState({ tempo: event.target.value });
                  }}
                ></input>
                <div className="input-group-append">
                  <span className="input-group-text">min</span>
                </div>
              </div>
            </div>
          </div>

          <div className="row ">
            <div className="col-12">
              <div className="form-group">
                <select
                  className="form-control form-control-lg"
                  onChange={this.handleSelectChange}
                  value={this.state.plano}
                >
                  <option>Selecione o plano</option>
                  <option value="30">FaleMais 30</option>
                  <option value="60">FaleMais 60</option>
                  <option value="120">FaleMais 120</option>
                </select>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-sm-6">
              <a
                href="#"
                className="btn btn-outline-success"
                onClick={this.submit}
              >
                Calculate
              </a>
            </div>
            <div className="col-sm-6">
            <span className="resultNoPromo">
                {this.showResult(this.state.ligacaoValorSemPromo)}
              </span>
              <span className="result">
                {this.showResult(this.state.ligacaoValor)}
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
