import React from 'react';
import Enzyme, { shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

import Calculadora from './Calculadora';

describe('<Calculator />', ()=>{
  it('renders some inputs', () => {
    const editor = shallow(<Calculadora />)
    expect(editor.find('select').length).toEqual(1);
  });
  it('output test', () => {
    const wrapper = shallow(<Calculadora />);
    const text = wrapper.find('span.result');
    expect(text.text()).toBe(showResult(0));
  });

  it('this shoud result R$ 0,0', () =>{
    let wrapper = shallow(<Calculadora  
      dddA="011" dddB="016" tempo={20} plano="30"
    />);
    const button = wrapper.find('a.btn');
    button.simulate('click');
    const text = wrapper.find('span.result');
    expect(text.text()).toBe(showResult(0.00));
  });

  it('this shoud result R$ 37,40', () =>{
    let wrapper = shallow(<Calculadora  
      dddA="011" dddB="017" tempo={80} plano="60"
    />);
    const button = wrapper.find('a.btn');
    button.simulate('click');
    const text = wrapper.find('span.result');
    expect(text.text()).toBe(showResult(37.40));
  });

  it('this shoud result R$ 167,20', () =>{
    let wrapper = shallow(<Calculadora  
      dddA="018" dddB="011" tempo={200} plano="120"
    />);
    const button = wrapper.find('a.btn');
    button.simulate('click');
    const text = wrapper.find('span.result');
    expect(text.text()).toBe(showResult(167.20));
  });

  it('this shoud result -', () =>{
    let wrapper = shallow(<Calculadora  
      dddA="018" dddB="017" tempo={100} plano="30"
    />);
    const button = wrapper.find('a.btn');
    button.simulate('click');
    const text = wrapper.find('span.result');
    expect(text.text()).toBe(showResult("R$ -"));
  });


  function showResult(result){
    if(typeof result == "number")
      return result.toLocaleString('pt-BR',  { style: 'currency', currency: 'BRL' });
    else
      return result;
  }
  

});
