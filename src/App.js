import React from 'react';
import logo from './logo.svg';
import './App.css';
import { callExpression } from '@babel/types';


import img1 from './assets/sunset-blur.0d5884c088d87a13ff7f.jpg'
import Calculadora from './components/Calculadora/Calculadora'

function App() {
  return (
    <div className="App">
      <div className="row justify-content-center">
          <Calculadora tempo="20" dddA="018" dddB="011" />
      </div>
      
    </div>
  );
}

export default App;
